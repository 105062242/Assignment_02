# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* Loading畫面
    * 我在遊戲一開始先載入所有物件，為了讓畫面不要太無聊，加了Loading的字樣和一條會跑的生命條。
    * Loading 全部跑完後，就進入開始畫面。
* 開始畫面
    * 進入開始畫面後，畫面上會有排名前五的玩家和分數，按Enter後可以開始遊戲。
* 遊戲畫面
    * 左右鍵控制玩家。
    * 玩家會依照目前狀態做出不同的動畫，像是左走、右走、掉落等等。
    * 平台分成一般的、有刺、彈簧、左右運輸帶、翻轉，共六種平台。
    * 玩家受到傷害後，身體會出現紅色閃爍，而且會有音效。
    * 採到平台有音效。
    * 有背景音樂。
    * 玩家死掉的時候也會出現音效。
    * 玩家的生命值會反映在血條上。
    * 玩家的分數會隨著遊戲的時間慢慢變多，平台也會越動越快。
* 死亡畫面
    * 死亡後，會出現對話框，讓玩家輸入自己的名字。
    * 死亡後按Enter即可回到開始畫面。