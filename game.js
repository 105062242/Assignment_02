var mainState ={
    preload: function()
    {
        this.falling_death_wav = game.add.audio('falling_death');
        this.injured_death_wav = game.add.audio('injured_death');
        this.injured_wav = game.add.audio('injured');
        this.bgm = game.add.audio('bgm',1,true);
        this.crush = game.add.audio('crush',0.5,false);
    },
    create: function()
    {
        game.stage.backgroundColor= '#000000';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        /*---   天花板的刺   ---*/
        this.ceiling = game.add.sprite(0,72,'ceiling');
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.body.setSize(400, 15, 0, -15);
        this.ceiling.body.immovable = true;
        this.ceiling.body.checkCollision.up = false;
        /*---   天花板的刺   ---*/


        /*---   牆壁   ---*/
        this.leftwall = game.add.sprite(0,72,'wall');
        game.physics.arcade.enable(this.leftwall);
        this.leftwall.body.immovable = true;
        
        this.rightwall = game.add.sprite(383,72,'wall');
        game.physics.arcade.enable(this.rightwall);
        this.rightwall.body.immovable = true;
        /*---   牆壁   ---*/


        /*---   看板   ---*/
        var right_board = [];
        var cons = 18;
        for(var i=0;i<9;++i)
            right_board[i] = game.add.sprite(400 + cons*i,72,'wall');
        
        /*---   看板   ---*/

        /*---   第一個平台    ---*/ 

        var first_platform;
        first_platform = game.add.sprite(150, 400, 'normal');
        game.physics.arcade.enable(first_platform);
        first_platform.body.immovable = true;
        first_platform.body.velocity.y = -150;
        first_platform.body.checkCollision.down = false;
        first_platform.body.checkCollision.left = false;
        first_platform.body.checkCollision.right = false;
        platforms.push(first_platform);
        /*---   第一個平台    ---*/ 
        
        //console.log(time1);

        /*--- 普通平台 ---*/
        this.normal_sample = [];
        this.normal_used = 0;
        for(var i=0;i<platform_number_MAX;++i)
        {
            this.normal_sample[i] = game.add.sprite(200, 600, 'normal');
            game.physics.arcade.enable(this.normal_sample[i]);
            this.normal_sample[i].body.checkCollision.down = false;
            this.normal_sample[i].body.checkCollision.left = false;
            this.normal_sample[i].body.checkCollision.right = false;
            this.normal_sample[i].body.immovable = true;
        }
        /*--- 普通平台 ---*/
        
        /*--- 釘子平台 ---*/
        this.nail_sample = [];
        this.nail_used = 0;
        for(var i=0;i<platform_number_MAX;++i)
        {
            this.nail_sample[i] = game.add.sprite(200, 600, 'nails');
            game.physics.arcade.enable(this.nail_sample[i]);
            this.nail_sample[i].body.checkCollision.down = false;
            this.nail_sample[i].body.checkCollision.left = false;
            this.nail_sample[i].body.checkCollision.right = false;
            this.nail_sample[i].body.setSize(96, 15, 0, 15);
            this.nail_sample[i].body.immovable = true;
        }
        /*--- 釘子平台 ---*/

        /*--- 彈簧平台 ---*/
        this.trampoline_sample = [];
        this.trampoline_used = 0;
        for(var i=0;i<platform_number_MAX;++i)
        {
            this.trampoline_sample[i] = game.add.sprite(200, 600, 'trampoline');
            game.physics.arcade.enable(this.trampoline_sample[i]);
            this.trampoline_sample[i].body.checkCollision.down = false;
            this.trampoline_sample[i].body.checkCollision.left = false;
            this.trampoline_sample[i].body.checkCollision.right = false;
            
            this.trampoline_sample[i].animations.add('jump', [ 4, 5, 0, 1, 2, 3], 16);
            this.trampoline_sample[i].frame = 3;
            this.trampoline_sample[i].body.immovable = true;
        }
        /*--- 彈簧平台 ---*/

        /*--- 假的平台 ---*/
        this.fake_sample = [];
        this.fake_used = 0;
        for(var i=0;i<platform_number_MAX;++i)
        {
            this.fake_sample[i] = game.add.sprite(200, 600, 'fake');
            game.physics.arcade.enable(this.fake_sample[i]);
            this.fake_sample[i].body.checkCollision.down = false;
            this.fake_sample[i].body.checkCollision.left = false;
            this.fake_sample[i].body.checkCollision.right = false;
            this.fake_sample[i].animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            this.fake_sample[i].body.immovable = true;
        }
        /*--- 假的平台 ---*/

        /*--- 左移平台 ---*/
        
        this.conveyor_left_sample = [];
        this.conveyor_left_used = 0;
        for(var i=0;i<platform_number_MAX;++i)
        {
            this.conveyor_left_sample[i] = game.add.sprite(200, 600, 'conveyor_left');
            game.physics.arcade.enable(this.conveyor_left_sample[i]);
            this.conveyor_left_sample[i].body.checkCollision.down = false;
            this.conveyor_left_sample[i].body.checkCollision.left = false;
            this.conveyor_left_sample[i].body.checkCollision.right = false;
            this.conveyor_left_sample[i].animations.add('scroll_left', [0, 1, 2, 3], 16, true);
            this.conveyor_left_sample[i].body.immovable = true;
        }
        /*--- 左移平台 ---*/

        /*--- 右移平台 ---*/
        
        this.conveyor_right_sample = [];
        this.conveyor_right_used = 0;
        for(var i=0;i<platform_number_MAX;++i)
        {
            this.conveyor_right_sample[i] = game.add.sprite(200, 600, 'conveyor_right');
            game.physics.arcade.enable(this.conveyor_right_sample[i]);
            this.conveyor_right_sample[i].body.checkCollision.down = false;
            this.conveyor_right_sample[i].body.checkCollision.left = false;
            this.conveyor_right_sample[i].body.checkCollision.right = false;
            this.conveyor_right_sample[i].animations.add('scroll_right', [0, 1, 2, 3], 16, true);
            this.conveyor_right_sample[i].body.immovable = true;
        }
        /*--- 右移平台 ---*/

        /*---   小朋友   ---*/
        this.player = game.add.sprite(200, 100, 'player');
        game.physics.arcade.enable(this.player);

        this.player.animations.add('leftwalk', [0, 1, 2, 3], 20);
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 20);
        this.player.animations.add('leftfly', [18, 19, 20, 21], 20);
        this.player.animations.add('rightfly', [27, 28, 29, 30], 20);
        this.player.animations.add('fly', [36, 37, 38, 39], 20);

        this.player.animations.add('leftwalk_RED', [4, 5, 6, 7], 20);
        this.player.animations.add('rightwalk_RED', [13, 14, 15, 16], 20);
        this.player.animations.add('leftfly_RED', [22, 23, 24, 25], 20);
        this.player.animations.add('rightfly_RED', [31, 32, 33, 34], 20);
        this.player.animations.add('fly_RED', [40, 41, 42, 43], 20);
        this.player.animations.add('steady_RED',[8, 17, 8, 17],20);
        this.player.frame = 8;
        this.player.body.gravity.y = 500;
        this.player.life = 10;
        /*---   小朋友   ---*/

        /*--- 上壁 ---*/
        //var upper_wall = [];
        this.upper_wall = [];
        for(var i=0;i<4;++i)
        {
            this.upper_wall[i] = game.add.sprite(-17 ,cons*i,'upper_wall');
            this.upper_wall[i+4] = game.add.sprite(-17 + 400 ,cons*i,'upper_wall');
        }
        /*
        game.physics.arcade.enable(this.upper_wall[1]);
        this.upper_wall.body.immovable = true;*/
        /*--- 上壁 ---*/

        /*---  血量  ---*/
        for(var i=0;i<10;i++)
            bloodBar[i] = game.add.sprite(startX + 10 + 8*i,startY,'filledBar');
        /*---  血量  ---*/
        
        /*--- 關卡 ---*/ 
        this.level_hundred = game.add.sprite(0,-10,'number_logo_0');
        this.level_ten = game.add.sprite(50,-10,'number_logo_0');
        this.level_one = game.add.sprite(100,-10,'number_logo_1');
        /*--- 關卡 ---*/ 
        
        /*--- 音效 ---*/
        this.falling_death_wav = game.add.audio('falling_death');
        this.injured_death_wav = game.add.audio('injured_death');
        this.injured_wav = game.add.audio('injured');
        /*--- 音效 ---*/

        time1 = game.time.now;
        this.Gethurt = false;
        this.initial_time = time1;
        this.plate_speed = -150;
        this.level = 1;
        this.bgm.play();
    },

    update: function() 
    {
        var time2 = game.time.now;
        //console.log(this.player.life + "," + this.Gethurt);
        if(this.player.life > 0 && this.player.body.y < 500)
        {
            game.physics.arcade.collide(this.player, this.leftwall);
            game.physics.arcade.collide(this.player, this.rightwall);
            game.physics.arcade.collide(this.player, this.ceiling,this.touchCeiling,null ,this);
            game.physics.arcade.collide(this.player, platforms, this.respond ,null ,this);
            this.movePlayer();
            

            if(this.Gethurt)
            {
                if(time2 - time_hurt > 800)
                    this.Gethurt = false;
            }
            if(time2 - time1 > 600)
            {
                time1 = time2;
                //console.log(time1);
                this.UpadatePlatform();
            }

            if(this.level < 15)
            {
                if( (time2 - this.initial_time) / 600 > this.level)
                {
                    this.initial_time = time2;
                    this.level += 1;
                    this.plate_speed += 1;
                    console.log(this.level);
                    this.refresh_level();
                }
            }
            else
            {
                if((time2 - this.initial_time) / 600 > 15)
                {
                    this.initial_time = time2;
                    this.level += 1;
                    this.plate_speed += 1;
                    console.log(this.level);
                    this.refresh_level();
                }
            }
        }
        else
        {
            this.player.body.gravity.y = 0;
            this.player.body.velocity.y = 0;
            this.player.body.velocity.x = 0;
            if(this.player.body.y > 500) this.falling_death_wav.play();
            else this.injured_death_wav.play();
            platforms.splice(0, platforms.length);
            final_level = this.level;


            this.bgm.stop();
            if(time2-time1 > 1000)    game.state.start('over');

            
        }
        
            
        
    },
    
    movePlayer: function()
    {
        var flying = false;
        if(this.player.touchOn !== undefined)
        {
             console.log("ertyui");
            if(this.player.body.velocity.y != this.player.touchOn.body.velocity.y) flying = true;
        }   
        else
        {
            if(this.player.body.velocity.y != -150)   flying = true;
        }
        console.log(this.player.touchOn);

        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
        {
            this.player.body.velocity.x = -300;
            this.player.facingLeft = true;
            if(this.Gethurt)
                if(!flying)this.player.animations.play('leftwalk_RED');
                else       this.player.animations.play('leftfly_RED');
            else
                if(!flying)this.player.animations.play('leftwalk');
                else       this.player.animations.play('leftfly');
        }
        else if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
        { 
            this.player.body.velocity.x = 300;
            this.player.facingLeft = false;
            if(this.Gethurt)
                if(!flying)this.player.animations.play('rightwalk_RED');
                else       this.player.animations.play('rightfly_RED');
            else
                if(!flying)this.player.animations.play('rightwalk');
                else       this.player.animations.play('rightfly');
        }    
        else
        {
            this.player.body.velocity.x = 0;
            
            if(this.Gethurt)
                if(!flying) this.player.animations.play('steady_RED');
                else        this.player.animations.play('fly_RED');
            else
                if(!flying)
                {
                    this.player.frame = 8;
                    this.player.animations.stop();
                }
                else this.player.animations.play('fly');

        }
        
    },
    UpadatePlatform: function()
    {
        var randX = Math.random()*270 + 17;
        var randPlat = Math.random()*16;

        var platform = this.create_a_platform(randPlat);

        platform.body.x = randX;
        platform.body.y = 510;
        platform.body.velocity.y = this.plate_speed;
        
        if(platforms.length > 20) platforms.splice(0, 1);
        platforms.push(platform);
        
    },
    touchCeiling: function(player, ceiling)
    {
        if(this.player.life > 4)
        {
            this.injured_wav.play();
            for(var i=0;i<4;i++)
            {
                bloodBar[this.player.life-i-1].destroy();
                bloodBar[this.player.life-i-1] = game.add.sprite(startX + 10 + 8*(this.player.life-i-1),startY,'emptyBar');
            }
        }
        
            
        player.life -= 4;

        this.Gethurt = true;
        time_hurt = game.time.now;

        ceiling.body.checkCollision.up = false;
        player.touchOn.body.checkCollision.up = false;
        
    },
    respond: function(player, platform)
    {
        
        if(platform.key == 'normal')                this.normal_respond(platform);
        else if(platform.key == 'nails')            this.nail_respond(platform);
        else if(platform.key == 'trampoline')       this.trampoline_respond(platform);
        else if(platform.key == 'fake')             this.fake_respond(platform);
        else if(platform.key == 'conveyor_left')    this.conveyor_left_respond(platform);
        else if(platform.key == 'conveyor_right')   this.convey_right_respond(platform);
        
    },

    normal_respond: function(platform)
    {
        if(platform !== this.player.touchOn)
        {
            this.crush.play();
            if(this.player.life < 10)
            {
                bloodBar[this.player.life] = game.add.sprite(startX + 10 + 8*this.player.life,startY,'filledBar');
                this.player.life += 1;
            }
            this.player.touchOn = platform;
        }
    },
    nail_respond: function(platform)
    {
        
        if(platform !== this.player.touchOn)
        {
            this.crush.play();
            if(this.player.life > 4)
            {
                this.injured_wav.play();
                for(var i=0;i<4;i++)
                {
                    bloodBar[this.player.life-i-1].destroy();
                    bloodBar[this.player.life-i-1] = game.add.sprite(startX + 10 + 8*(this.player.life-i-1),startY,'emptyBar');
                }
            }
                
            this.player.life -= 4;
            this.player.touchOn = platform;
            this.Gethurt = true;
            time_hurt = game.time.now;
        }
    },
    trampoline_respond: function(platform)
    {
        if(this.player.life < 10)
        {
            bloodBar[this.player.life] = game.add.sprite(startX + 10 + 8*this.player.life,startY,'filledBar');
            this.player.life += 1;
        }
        this.crush.play();
        if(platform !== this.player.touchOn)
        {
            
            this.player.touchOn = platform;
        }

        platform.animations.play('jump');
        this.player.body.velocity.y = -400;
    },
    fake_respond: function(platform)
    {
        if(platform !== this.player.touchOn)
        {
            this.crush.play();
            if(this.player.life < 10)
            {
                bloodBar[this.player.life] = game.add.sprite(startX + 10 + 8*this.player.life,startY,'filledBar');
                this.player.life += 1;
            }
            this.player.touchOn = platform;
        }

        platform.animations.play('turn');

        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);

        setTimeout(function() {
            platform.body.checkCollision.up = true;
        }, 1000);
    },
    conveyor_left_respond: function(platform)
    {
        if(platform !== this.player.touchOn)
        {
            this.crush.play();
            if(this.player.life < 10)
            {
                bloodBar[this.player.life] = game.add.sprite(startX + 10 + 8*this.player.life,startY,'filledBar');
                this.player.life += 1;
            }
            
            this.player.touchOn = platform;
        }

        this.player.body.x -= 2;
    },
    convey_right_respond: function(platform)
    {
        if(platform !== this.player.touchOn)
        {
            this.crush.play();
            if(this.player.life < 10)
            {
                bloodBar[this.player.life] = game.add.sprite(startX + 10 + 8*this.player.life,startY,'filledBar');
                this.player.life += 1;
            }
            this.player.touchOn = platform;
        }
        
        this.player.body.x += 2;
    },
    create_a_platform: function(randPlat)
    {
        var platform;
        if(randPlat < 6)            //normal
        {
            platform = this.normal_sample[this.normal_used];
            this.normal_used = (this.normal_used+1)%platform_number_MAX;
        }
        else if(randPlat < 8)       //nails
        {
            platform = this.nail_sample[this.nail_used];
            this.nail_used = (this.nail_used+1)%platform_number_MAX;
        }
        else if(randPlat < 10)      //trampoline
        {
            platform = this.trampoline_sample[this.trampoline_used];
            this.trampoline_used = (this.trampoline_used+1)%platform_number_MAX;
        }  
        else if(randPlat < 12)      //fake
        {
            platform = this.fake_sample[this.fake_used];
            this.fake_used = (this.fake_used+1)%platform_number_MAX; 
        }
        else if(randPlat < 14)      //conveyor_left
        {
            platform = this.conveyor_left_sample[this.conveyor_left_used];
            this.conveyor_left_used = (this.conveyor_left_used+1)%platform_number_MAX;
            platform.animations.play('scroll_left');
        }
        else                        //conveyor_right
        {
            platform = this.conveyor_right_sample[this.conveyor_right_used];
            this.conveyor_right_used = (this.conveyor_right_used+1)%platform_number_MAX;
            platform.animations.play('scroll_right');
        }

        return platform;
    },
    refresh_level: function()
    {
        var hundred = this.level/100;
        var ten = (this.level%100)/10;
        var one = this.level%10;

        this.level_hundred.destroy();
        this.level_ten.destroy();
        this.level_one.destroy();

        this.level_hundred = game.add.sprite(0,-10,'number_logo_' + parseInt(hundred));
        this.level_ten = game.add.sprite(50,-10,'number_logo_' + parseInt(ten));
        this.level_one = game.add.sprite(100,-10,'number_logo_' +  parseInt(one));
        
    }
};


var game = new Phaser.Game(545, 472, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.add('begin',beginState);
game.state.add('over',overState);
game.state.add('load',loadState);
game.state.add('preload',preloadState);
game.state.start('preload');
//game.state.start('begin');
