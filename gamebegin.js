var preloadState ={
    preload:function()
    {
        game.load.image('healthbar', 'images/healthbar.png');
        game.load.image('health_empty_bar', 'images/health_empty_bar.png');
    },
    create:function()
    {
        game.state.start('load');
    }
}
var loadState ={
    preload:function()
    {
        
        var loading = game.add.text(game.width/2, 150,'loading...', { font: '22px Arial', fill: '#ffffff' });
        loading.anchor.setTo(0.5, 0.5);
        // // Display the progress bar
        var health_empty_bar = game.add.sprite(game.width/2 - 48, 200, 'health_empty_bar');
        var loading_Bar = game.add.sprite(game.width/2 - 48, 200, 'healthbar');
        game.load.setPreloadSprite(loading_Bar);






        game.load.image('wall', 'images/wall.png');
        game.load.image('ceiling', 'images/ceiling.png');
        game.load.image('upper_wall','images/wall_upper.png');


        game.load.image('normal', 'images/normal.png');
        game.load.image('nails', 'images/nails.png');
        game.load.spritesheet('conveyor_right', 'images/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyor_left', 'images/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'images/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'images/fake.png', 96, 36);
        game.load.spritesheet('emptyBar', 'images/emptybar.png', 8, 16);
        game.load.spritesheet('filledBar', 'images/filledbar.png', 8, 16);
        game.load.spritesheet('player', 'images/player.png',32, 32);
        for(var i=0;i<10;++i)    game.load.image('number_logo_' + i, 'images/numbers/' + i + '.png');


        game.load.audio('falling_death', 'soundeffect/falling_death.wav');
        game.load.audio('injured_death', 'soundeffect/injured_death.wav');
        game.load.audio('injured', 'soundeffect/injured.wav');
        game.load.audio('bgm', 'soundeffect/starrynight.mp3');
        game.load.audio('crush', 'soundeffect/crush.mp3');
    },
    create: function()
    {
        game.state.start('begin');
    }
}











var beginState ={
    preload:function()
    {
        
        
    },
    create: function()
    {
        game.stage.backgroundColor= '#000000';
        this.ceiling = game.add.sprite(0,72,'ceiling');
        this.leftwall = game.add.sprite(0,72,'wall');
        this.rightwall = game.add.sprite(383,72,'wall');


        var cons = 18;
        var right_board = [];
        for(var i=0;i<9;++i)    right_board[i] = game.add.sprite(400 + cons*i,72,'wall');
        
       
        this.upper_wall = [];
        for(var i=0;i<4;++i)
        {
            this.upper_wall[i] = game.add.sprite(-17 ,cons*i,'upper_wall');
            this.upper_wall[i+4] = game.add.sprite(-17 + 400 ,cons*i,'upper_wall');
        }
        var scoreboard = game.add.text(200, game.height/2 + 50,'loading...' , { font: '25px Arial', fill: '#ffffff' });
        scoreboard.anchor.setTo(0.5, 0.5);

        var database = firebase.database().ref('player_list').orderByChild('score').limitToLast(5);
        database.once('value', function(snapshot){
            var score = [];
            var i=0;
            snapshot.forEach(function(childshot){

                score[i]= + 5-i + '. ' + childshot.val().player_name + '\t' + childshot.val().score + '\n';
                ++i;
            });
            score[i] = 'Enter to start\n\n   Scorboard\n';
            scoreboard.text = "";
            for(i=score.length-1;i>=0;--i)
            {
                scoreboard.text += score[i];
            }
             
        });


    },
    update: function()
    {
        this.StartGame();
    },

    StartGame: function()
    {
        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER))
        {
            game.state.start('main');
        }
    }

}